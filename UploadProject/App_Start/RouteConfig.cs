﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace UploadProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "IframeUrl",
            //    url: "Home/IframeUrl/{id}",
            //    defaults: new { controller = "Home", action = "IframeUrl" }
            //);
            //routes.MapRoute(
            //    name: "Iframe2",
            //    url: "Home/Iframe2/{id}/{parentType}",
            //    defaults: new { controller = "Home", action = "Iframe2" }
            //); routes.MapRoute(
            //     name: "Iframe3",
            //     url: "Home/Iframe3/{id}/{parentType}",
            //     defaults: new { controller = "Home", action = "Iframe3" }
            // ); routes.MapRoute(
            //     name: "Iframe4",
            //     url: "Home/Iframe4/{id}/{parentType}",
            //     defaults: new { controller = "Home", action = "Iframe4" }
            // );
            //routes.MapRoute(
            //    name: "Iframe1",
            //    url: "Home/Iframe1/{id}/{parentType}",
            //    defaults: new { controller = "Home", action = "Iframe1" }
            //);
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
            );
        }
    }
}
