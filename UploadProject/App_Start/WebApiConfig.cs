﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;


namespace UploadProject.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.EnableCors();

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
               name: "GetParents",
               routeTemplate: "api/Uploader/GetParents/{application_id}/",
               defaults: new { Controller = "Uploader", Action = "GetParents" }
            );
            config.Routes.MapHttpRoute(
               name: "GetLinks",
               routeTemplate: "api/Uploader/GetLinksFromDB/{application_id}/",
               defaults: new { Controller = "Uploader", Action = "GetLinksFromDB" }
            );
            
            config.Routes.MapHttpRoute(
               name: "ApiTest",
               routeTemplate: "api/Uploader/ApiTest/",
               defaults: new { Controller = "Uploader", Action = "ApiTest" }
            );
            config.Routes.MapHttpRoute(
               name: "AddLinksToDB",
               routeTemplate: "api/Uploader/AddLinksToDB/",
               defaults: new { Controller = "Uploader", Action = "AddLinksToDB" }
            );

            config.Routes.MapHttpRoute(
               name: "ChangeStatus",
               routeTemplate: "api/Uploader/AddUpdated/{application_id}/{parent_type}",
               defaults: new { Controller = "Uploader", Action = "AddUpdated" }
            );

            config.Routes.MapHttpRoute(
               name: "Uploader",
               routeTemplate: "api/Uploader/Upload/",
               defaults: new { Controller = "Uploader", Action = "Upload" }
            );

        }
    }
}