﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UploadProject.Models
{
    public class tblUsers {

        [Key]
        public int user_id { get; set; }
        public string user_id_desc { get; set; }
    }
    
    public class tblAffidavitOfFinancialSupportStatingMonthlySupportProvided
    {
        [Key]
        public int user_id { get; set; }
        public string date_issued { get; set; }
        public string id_presented_to_the_notary_public { get; set; }
        public string place_of_isse { get; set; }
    }
    public class tblCertificateOfNonFilingOfIncomeTaxReturnOR
    {
        [Key]
        public int user_id { get; set; }
        public string date_issued { get; set; }
        public string issuing_BIR_branch { get; set; }
        public string name_of_issuing_authority_signatory { get; set; }

    }
    public class tblMunicipalCertificateOfUnemployment
    {
        [Key]
        public int user_id { get; set; }
        public string date_issued { get; set; }
        public string name_of_issuing_authority_signatory { get; set; }
        public string city_municipality { get; set; }
    }

    public class tblLatestAnnualIncomeTaxReturnOR
    {
        [Key]
        public int user_id { get; set; }
        public string year_covered { get; set; }
        public string TIN { get; set; }
        public string gross_compensation_income { get; set; }
    }

    public class tblCertificateOfEmployment
    {
        [Key]
        public int user_id { get; set; }
        public string company_name { get; set; }
        public string date_issued { get; set; }
        public string total_income_declared { get; set; }
    }
    public class checkifuploaded {
        [Key]
        public int uploadId { get; set; }
        public int applicationId { get; set; }
        public string parent_type { get; set; }
        public string date_uploaded { get; set; }
        public int upload_count { get; set; }
    }

    public class Links {
        [Key]
        public int link_id { get; set; }
        public int applicationId { get; set; }
        public string fileName { get; set; }
        public string URL { get; set; }
        public string date_created { get; set; }
        public string parent_type { get; set; }
    }
}