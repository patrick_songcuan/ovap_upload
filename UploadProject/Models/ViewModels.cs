﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UploadProject.Models
{
    public class Users
    {
        public string user_fname { get; set; }

    }

    public class EmployedInThePhilippines
    {
        public LatestAnnualIncomeTaxReturnOR latest_annual_income { get; set; }
        public CertificateOfEmployment certificate_of_employment { get; set; }
    }

    public class EmployedAbroad {
        public CertificateOfEmployment certificate_of_employment { get; set; }
    }

    public class UnemployedAndWithoutIncome {
        public CertificateOfNonFilingOfIncomeTaxReturnOR certificate_of_non_filing_of_income_tax_return_or { get; set; }
        public MunicipalCertificateOfUnemployment municipal_certification_of_unemployment { get; set; }
    }

    public class UnemployedButWithOtherSourcesOfIncome {
        public AffidavitOfFinancialSupportStatingMonthlySupportProvided affidavit_of_financial_support_stating_monthly_support_provided { get; set; }
    }

    public class AffidavitOfFinancialSupportStatingMonthlySupportProvided {
        public string date_issued { get; set; }
        public string id_presented_to_the_notary_public { get; set; }
        public string place_of_isse { get; set; }
    }
    public class CertificateOfNonFilingOfIncomeTaxReturnOR {
        public string date_issued { get; set; }
        public string issuing_BIR_branch { get; set; }
        public string name_of_issuing_authority_signatory { get; set; }

    }
    public class MunicipalCertificateOfUnemployment
    {
        public string date_issued { get; set; }
        public string name_of_issuing_authority_signatory { get; set; }
        public string city_municipality { get; set; }
    }

    public class LatestAnnualIncomeTaxReturnOR
    {
        public string year_covered { get; set; }
        public string TIN { get; set; }
        public string gross_compensation_income { get; set; }
    }

    public class CertificateOfEmployment
    {
        public string company_name { get; set; }
        public string date_issued { get; set; }
        public string total_income_declared { get; set; }
    }

    public class PostLinks
    {
        public int applicationId { get; set; }
        public string fileName { get; set; }
        public string URL { get; set; }
        public string DateUploaded { get; set; }
        public string ParentType { get; set; }
    }
}