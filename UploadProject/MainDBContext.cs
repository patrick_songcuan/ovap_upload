﻿
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web;
using UploadProject.Models;

namespace UploadProject
{
    public class MainDBContext : DbContext
    {


        public MainDBContext()
                : base("name=prod")
        {

        }
        protected override void OnModelCreating(DbModelBuilder dbModelBuilder)
        {
            dbModelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<checkifuploaded> checkifuploaded { get; set; }
        public DbSet<Links> Links { get; set; }
    }
 }