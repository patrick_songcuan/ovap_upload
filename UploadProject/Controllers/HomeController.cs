﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UploadProject.Models;

namespace UploadProject.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadInit() {
            return View();
        }
        [HttpGet]
        public ActionResult UploadForm1()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadForm1(EmployedInThePhilippines eipModel,string id, string parentType)
        {
            return RedirectToAction("Iframe1",new { id = id,parentType = parentType });
        }
        [HttpGet]
        public ActionResult UploadForm2()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadForm2(EmployedAbroad eaModel, string id, string parentType)
        {
            return RedirectToAction("Iframe2", new { id = id, parentType = parentType });
        }

        [HttpGet]
        public ActionResult UploadForm3()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadForm3(EmployedAbroad eaModel, string id, string parentType)
        {
            return RedirectToAction("Iframe3", new { id = id, parentType = parentType });
        }

        [HttpGet]
        public ActionResult UploadForm4()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadForm4(EmployedAbroad eaModel, string id, string parentType)
        {
            return RedirectToAction("Iframe4", new { id = id, parentType = parentType });
        }
        [HttpGet]
        public ActionResult Iframe1(string id, string parentType) {
            try
            {
                ViewBag.id = id;
                ViewBag.parentType = parentType;
                return View();
            }
            catch (Exception) {
                return RedirectToAction("UploadForm1");
            }
        }
        [HttpGet]
        public ActionResult Iframe2(string id, string parentType)
        {
            try
            {
                ViewBag.id = id;
                ViewBag.parentType = parentType;
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("UploadForm2");
            }
        }
        [HttpGet]
        public ActionResult Iframe3(string id, string parentType)
        {
            try
            {
                ViewBag.id = id;
                ViewBag.parentType = parentType;
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("UploadForm3");
            }
        }
        [HttpGet]
        public ActionResult Iframe4(string id, string parentType)
        {
            try
            {
                ViewBag.id = id;
                ViewBag.parentType = parentType;
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("UploadForm3");
            }
        }

        [HttpGet]
        public ActionResult GetFiles()
        {
            return View();
        }

        [HttpGet]
        public ActionResult IframeUrl(string id)
        {
            ViewBag.id = id;
            return View();
        }
    }
}