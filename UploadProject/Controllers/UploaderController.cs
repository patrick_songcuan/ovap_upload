﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using UploadProject.Models;

namespace UploadProject.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UploaderController : ApiController
    {

        [HttpGet]
        public IHttpActionResult AddUpdated(int application_id, string parent_type)
        {
            MainDBContext db = new MainDBContext();

            if (db.checkifuploaded.Any(u => u.applicationId == application_id && u.parent_type == parent_type))
            {
                var updateTable = db.checkifuploaded.Where(u => u.applicationId == application_id && u.parent_type == parent_type).First();
                updateTable.date_uploaded = DateTime.Now.AddHours(16).ToString();
                updateTable.upload_count = updateTable.upload_count + 1;
                db.SaveChanges();
                // if app id and parent type exists
                return Ok(200);
            }
            else
            {
                var uploadTable = db.checkifuploaded.Create();
                uploadTable.applicationId = application_id;
                uploadTable.date_uploaded = DateTime.Now.AddHours(16).ToString();
                uploadTable.parent_type = parent_type;
                uploadTable.upload_count = 1;
                db.checkifuploaded.Add(uploadTable);
                db.SaveChanges();


                return Ok(200);
            }

        }

        [HttpPost]
        public IHttpActionResult AddLinksToDB(PostLinks links)
        {
            MainDBContext db = new MainDBContext();
            if (db.Links.Any(u => u.URL == links.URL))
            {
                return Ok(200);
            }
            else
            {
                var linkList = db.Links.Create();
                linkList.applicationId = links.applicationId;
                linkList.fileName = links.fileName;
                linkList.URL = links.URL;
                linkList.date_created = links.DateUploaded;
                linkList.parent_type = links.ParentType;
                db.Links.Add(linkList);
                db.SaveChanges();
                return Ok(200);
            }
        }

        [HttpGet]
        public IHttpActionResult ApiTest()
        {
            
                return Ok(200);
            
        }

        [HttpGet]
        public IHttpActionResult GetLinksFromDB(int application_id)
        {
            MainDBContext db = new MainDBContext();
            var Links = db.Links.Where(u => u.applicationId == application_id);
            return Ok(new { Links });

        }

        [HttpGet]
        public IHttpActionResult GetParents(int application_id)
        {
            MainDBContext db = new MainDBContext();
            var Links = db.Links.Where(u => u.applicationId == application_id).Select(u => u.parent_type).Distinct();
            return Ok(new { Links });

        }

    }
}
